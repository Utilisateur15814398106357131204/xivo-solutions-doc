*************************************
Old Pack Reporting Upgrade Procedures
*************************************

These notes include upgrade procedures for old versions of the **Pack reporting**, before **XivoCC** starts and before it was packaged with Docker.
In those cases, run the following command to find the installed version of the pack reporting:

.. code-block:: bash

   dpkg -l|grep pack-reporting

From version < 1.6
==================

* data retention time will be lost during upgrade : save it and write it back in */etc/xivo-reporting-db.conf*
* the upgrade is likely to be long if there is a lot of data in *queue_log*. Purge old data out of this table if possible
  in order to accelerate the upgrade
* at the end of the upgrade, run *apt-get autoremove* (deletion of xivo-stat, xivo-libdao and xivo-lib-python)

From version < 1.8
==================

* XiVO in version < 14.08 is not supported anymore
* if it is required, the upgrade of the XiVO must be done before the upgrade of the pack reporting,
  and no call must be performed between the two upgrades

From a version using Debian packaging to a version using Docker
===============================================================

* **Beware**: this will require a migration of the original PostgreSQL database to the Dockerised one. For this you need to have free disk space :
  the amount of free disk space must equal the size of */var/lib/postgresql*. This check must be performed after docker images have been pulled.
* Run the following commands:

.. code-block:: bash

   apt-get update
   service xivo-db-replication stop
   service xivo-full-stats stopsource/releasenotes/index.rst
   apt-get install pack-reporting xivo-full-stats xivo-reporting-db xivo-db-replication db-utils
   service xivo-db-replication stop
   service xivo-full-stats stop

* Install docker, docker-compose and xivocc-installer
* Open `docker-xivocc.yml` and remove sections recording_rsync, config_mgt, recording_server, xuc, xucmgt
* Run `xivocc-dcomp pull`
* CHECK THE FREE DISK SPACE. The next command will migrate the database. This may take several hours.

.. code-block:: bash

   sudo -u postgres pg_dump --format c xivo_stats | docker exec -i xivocc_pgxivocc_1 pg_restore -U postgres -d xivo_stats

* Start by `xivocc-dcomp up -d`

From a dockerized version before callbacks
==========================================

* Run the following commands:

.. code-block:: bash

   docker exec -ti compose_pgxivocc_1 psql -U postgres -c 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp"' xivo_stats
   docker exec -ti compose_pgxivocc_1 psql -U postgres -c 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp"' xuc_rights
