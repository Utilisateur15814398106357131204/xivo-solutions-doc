.. _phone_api:

*********
Phone API
*********

This API allow you to remotely control a device once a user associated to the device through a line is authenticated.

A sample of implementation is available in `app/assets/javascripts/pages/sample.js`
and `app/views/sample/sample.scala.html`


Phone Methods
=============

Cti.setData(variables)
----------------------
Attach data to the device current calls. When there is a call connected to a device, Data can be attached by passing
key values as a json object `Cti.setData("{'var1':'val1','USR_var2':'val2'}");`

The folowing json message is then sent to the server :

::

        {"claz":"web","command":"setData","variables":{"var1":"val1","USR_var2":"val2"}}

When the call is transfered i.e. (`Cti.directTransfer(destination)`_), data is sent in the event ringing see `Phone Events`_,
and in subsequent events.
Data is not propagated in the reporting database.

::

         {"eventType":"EventRinging","DN":"1000","otherDN":"0427466347","linkedId":"1469709757.74","uniqueId":"1469709960.78","queueName":"bluesky",
            "userData":{    "XIVO_CONTEXT":"from-extern",
                            "XIVO_USERID":"1",
                            "USR_var1":"val1",
                            "USR_var2":"val2",
                            "XIVO_EXTENPATTERN":"_012305XXXX",
                            "XIVO_SRCNUM":"0427466347",
                            "XIVO_DST_FIRSTNAME":"Brucé",
                            "XIVO_DSTNUM":"0123053012","XIVO_DST_LASTNAME":"Wail"}}



Note that *USR\_* prefix is added to the key, if the key does not start with it. Only attached data beginning with *USR\_* are sent back to the client API.

  .. warning:: When transfering a call, these variables are attached to the new channel however to prevent propagation on all trunk channels, your trunk name must contain 'trunk' so they can be distinguished from sip devices.

.. _Cti.dial:

Cti.dial(destination, variables)
--------------------------------
Place a call to destination with the provided variables. Variables must take the following form:

::

    {
        var1: "value 1",
        var2: "value 2"
    }

USR_var1 and USR_var2 will be attached to the call and propagated to `Phone Events`_

Cti.dialFromMobile(destination, variables)
------------------------------------------
Place a call from logged user's mobile number to destination with the provided variables. Variables must take the following form:

::

    {
        var1: "value 1",
        var2: "value 2"
    }

USR_var1 and USR_var2 will be attached to the call and propagated to `Phone Events`_

When placing a call using this api, `Phone Events`_ will be sent through the websocket when calls state change.
If a call is made to a conference room hosted on the XiVO, :ref:`conference_events` will be sent also.

.. warning:: When entering a conference room **using this API**, if the conference room is configured with an administrator PIN, the user will **enter directly** the conference room **as an administrator** without having to enter a PIN code. If the conference room has only a user PIN, the user will also enter directly without having to enter a PIN code.

Cti.dialByUsername(username, variables)
------------------------------------------
Place a call to destination, defined by username, with the provided variables. Variables must take the following form:

::

    {
        var1: "value 1",
        var2: "value 2"
    }

USR_var1 and USR_var2 will be attached to the call and propagated to `Phone Events`_


Cti.originate(destination)
--------------------------
Originate a call

Cti.hangup(uniqueId)
--------------------
Hangup established call. *uniqueId* is optional, but if you set it, you can explicitly hangup a call.

Cti.answer(uniqueId)
--------------------
Answers a call.
*uniqueId* is optional, but if you set it, you can explicitly answer a call among other ringing calls. (however, this parameter is for **WebRTC only**)

Cti.hold(uniqueId)
------------------
Put current call on hold. *uniqueId* is optional.
If *uniqueId* is set it you can explicitly hold/unhold a call among other calls. (however, this parameter is for **WebRTC only**)

Note that you can't have two established. If you have one establish call and on call on hold, if you unhold this last call, the established will go automatically on hold.

Cti.directTransfer(destination)
-------------------------------
Tranfert to destination

Cti.attendedTransfer(destination)
---------------------------------
Start a transfer to a destination

Cti.completeTransfer()
----------------------
Complete previously started transfer

Cti.cancelTransfer()
--------------------
Cancel a transfer

.. _Cti.getCurrentCallsPhoneEvents:

Cti.getCurrentCallsPhoneEvents()
--------------------------------
Request PhoneEvents for current device calls. See `Phone Events`_ for answer description.


Cti.naFwd(destination,state)
----------------------------
Forward on non answer


Cti.uncFwd(destination,state)
-----------------------------
Unconditionnal forward


Cti.busyFwd(destination,state)
------------------------------
Forward on busy

Cti.dnd(state)
--------------
Set or unset do not disturb, state true or false

.. _phoneevents:

Phone Events
============

* Cti.MessageType.PHONEEVENT
* Cti.MessageType.CURRENTCALLSPHONEEVENTS

Phone events are automatically sent when application is connected.

Format

::

    {
        "msgType":"PhoneEvent",
        "ctiMessage":{
            "eventType":"EventRinging",
            "DN":"1118",
            "otherDN":"1058",
            "otherDName":"Jane Black",
            "linkedId":"1447670380.34",
            "uniqueId":"1447670382.37",
            "queueName":"blue",
            "callDirection": "Incoming",
            "userData":{
                "XIVO_CONTEXT":"default","XIVO_USERID":"9","XIVO_SRCNUM":"1058","XIVO_DSTNUM":"3000"
            },
            username: "jblack"
        }
    }

+--------------+----------------------------------------+
| fields       | Description                            |
+==============+========================================+
| Event types  | *  EventReleased                       |
|              | *  EventDialing                        |
|              | *  EventRinging                        |
|              | *  EventEstablished                    |
|              | *  EventOnHold                         |
+--------------+----------------------------------------+
| DN           | The directory number of the event      |
+--------------+----------------------------------------+
| otherDN      | Can be calling number of called number |
+--------------+----------------------------------------+
| otherDName   | Can be name of caller of called number |
+--------------+----------------------------------------+
| queueName    | Optional, the queue name for inbound   |
|              | acd calls                              |
+--------------+----------------------------------------+
| callDirection| Can be Incoming or Outgoing            |
+--------------+----------------------------------------+
| UserData     | Contains a list of attached data,      |
|              | system data XIVO\_ or data attached    |
|              | to the call key beginning by USR\_     |
+--------------+----------------------------------------+
| username     | Can be the username cti of called      |
|              | number, it's only defined when the     |
|              | called user is an internal user        |
+--------------+----------------------------------------+


If you use the following preprocess subroutine

::

    [user_data_test]
    exten = s,1,Log(DEBUG,**** set user data ****)
    same  =       n,SET(USR_DATA1=hello)
    same  =       n,SET(USR_DATA2=24)
    same  =       n,SET(USR_DATA3=with space)
    same  =       n,Return()

you will get these data in the events. Data can also be attached using the `Cti.dial`_ command.

You can also request a message with a concatenation of PhoneEvents for current calls by `Cti.getCurrentCallsPhoneEvents`_ command.
The response to this command is formatted as follows:

::

    {
        "msgType":"CurrentCallsPhoneEvents",
        "ctiMessage":{
            "events": [
	    {
              "eventType":"EventRinging",
              "DN":"1118",
              "otherDN":"1058",
              "otherDName":"Jane Black",
              "linkedId":"1447670380.34",
              "uniqueId":"1447670382.37",
              "queueName":"blue",
              "callDirection": "Incoming",
              "userData":{
                  "XIVO_CONTEXT":"default","XIVO_USERID":"9","XIVO_SRCNUM":"1058","XIVO_DSTNUM":"3000"
              }
	    },
            {
              "eventType":"EventEstablished",
              ...
            },
            ...
        }
    }

Phone Status Update
-------------------

When opening the websocket, the following message will be received automatically. It will indicate the current phone status (or the webrtc status) of the user and indicate if the user is ready to receive calls. A new event will be received if, for any reason, the phone (or the webrtc line) becomes unavailable.

Cti Message Type
  ``Cti.MessageType.PHONESTATUSUPDATE``

Example
  .. code-block:: javascript

    {
        "msgType":"PhoneStatusUpdate",
        "ctiMessage":{
            status: "AVAILABLE"
        }
    }


Possible ``status`` values are:

ONHOLD
  The phone has at least one held call

RINGING
  The phone is ringing

INDISPONIBLE
  The phone is unavailable and is not able to receive calls

BUSY_AND_RINGING
  The phone is busy and ringing

AVAILABLE
  The phone is available and ready to receive calls

CALLING
  The phone is on call

BUSY
  The phone is busy

DEACTIVATED
  The phone is deactivated and is not able to receive calls

UNEXISTING
  The phone does not exist

ERROR
  An error occured while monitoring the phone. The phone may not be able to receive calls


.. _Cti.subscribeToPhoneHints:


Phone Hint Status Methods
=========================

Cti.subscribeToPhoneHints(phoneNumbers)
---------------------------------------
* phoneNumbers (Array of string): list of phone numbers to subscribe

Subscribe to PhoneHintStatusEvents for a list of phone numbers. You will get an initial event with the current
Phone Hint Status (see `Phone Hint Status Events`_ for details) for every subscribed phone number and then a new
`Phone Hint Status Events`_ for every change to the phone number state. After the Xuc server restart it may happen
that the current phone state is unknown, in which case you will not get the initial event, only the first event update.
You can repeat the command to subscribe to more numbers. The subscription is valid for the current websocket and
can be cancelled either by closing the websocket, or by the `Cti.unsubscribeFromAllPhoneHints()`_ command.

Cti.unsubscribeFromAllPhoneHints()
----------------------------------
This command allows you to cancel all previous subscription to phone hints.

Phone Hint Status Events
========================

You can subscribe to PhoneHintStatusEvents using `Cti.subscribeToPhoneHints`_, allowing you to monitor state of the phone line with a given number.
PhoneHintStatusEvents has the following format:

::

    {
        "msgType":"PhoneHintStatusEvent",
        "ctiMessage":{
            "number":"1005","status":0
        }
    }

The phone hint status code can be resolved to a name using the `Cti.PhoneStatus` object in the `cti.js`. E.g.,
the status 0 from the example above stands for "AVAILABLE".

Voice Mail Status Events
========================

* VOICEMAILSTATUSUPDATE : "VoiceMailStatusUpdate",

::

    {"msgType":"VoiceMailStatusUpdate","ctiMessage":{"voiceMailId":58,"newMessages":2,"waitingMessages":1,"oldMessages":1}}
