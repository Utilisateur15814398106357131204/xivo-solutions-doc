.. _meetingrooms_config:

***************************
Meeting Rooms Configuration
***************************

.. contents:: :local:

Meeting Rooms configuration
===========================

Create default env file
-----------------------

#. Launch the following commands to add needed variables to :file:`/etc/docker/meetingrooms/.env` file:

   .. code-block:: bash

    function generatePassword() {
        openssl rand -hex 16
    }

    JICOFO_AUTH_PASSWORD=$(generatePassword)
    JVB_AUTH_PASSWORD=$(generatePassword)
    JIGASI_XMPP_PASSWORD=$(generatePassword)
    JIGASI_SIP_PASSWORD=$(generatePassword)

    cat >> /etc/docker/meetingrooms/.env << EOF
    # XiVO vars
    XIVOCC_TAG=2021.13
    XIVOCC_DIST=latest
    XUC_HOST=<FILL IN>
    MEETING_ROOMS_HOST_IP=<FILL IN>
    XIVO_HOST=<FILL IN>
    #
    # JWT Configuration
    ENABLE_AUTH=1
    ENABLE_GUESTS=0
    AUTH_TYPE=jwt
    JWT_APP_ID=xivo
    JWT_APP_SECRET=<FILL IN>
    #
    # Security
    #
    JICOFO_AUTH_PASSWORD=${JICOFO_AUTH_PASSWORD}
    JVB_AUTH_PASSWORD=${JVB_AUTH_PASSWORD}
    JIGASI_XMPP_PASSWORD=${JIGASI_XMPP_PASSWORD}
    #
    # Basic config
    #
    CONFIG=/etc/docker/meetingrooms/jitsi/
    DOCKER_HOST_ADDRESS=\${MEETING_ROOMS_HOST_IP}
    HTTP_PORT=80
    HTTPS_PORT=443
    TZ=Europe/Paris
    PUBLIC_URL=https://\${XUC_HOST}/video
    #
    # Basic Jigasi configuration options
    #
    JIGASI_SIP_URI=xivo-jitsi@\${XIVO_HOST}
    JIGASI_SIP_PASSWORD=${JIGASI_SIP_PASSWORD}
    JIGASI_SIP_SERVER=\${XIVO_HOST}
    JIGASI_SIP_PORT=5060
    JIGASI_SIP_TRANSPORT=UDP
    #
    # Advanced configuration options (you generally don't need to change these)
    #
    XMPP_DOMAIN=meet.jitsi
    XMPP_SERVER=xmpp.meet.jitsi
    XMPP_BOSH_URL_BASE=http://xmpp.meet.jitsi:5280
    XMPP_AUTH_DOMAIN=auth.meet.jitsi
    XMPP_MUC_DOMAIN=muc.meet.jitsi
    XMPP_MODULES=muc_size
    XMPP_INTERNAL_MUC_DOMAIN=internal-muc.meet.jitsi
    XMPP_GUEST_DOMAIN=guest.meet.jitsi
    JVB_BREWERY_MUC=jvbbrewery
    JVB_AUTH_USER=jvb
    JVB_STUN_SERVERS=meet-jit-si-turnrelay.jitsi.net:443
    JVB_PORT=10000
    JVB_TCP_HARVESTER_DISABLED=true
    JVB_TCP_PORT=4443
    JVB_TCP_MAPPED_PORT=4443
    JICOFO_AUTH_USER=focus
    JIGASI_XMPP_USER=jigasi
    JIGASI_BREWERY_MUC=jigasibrewery
    JIGASI_PORT_MIN=20000
    JIGASI_PORT_MAX=20050
    XMPP_RECORDER_DOMAIN=recorder.meet.jitsi
    JIBRI_RECORDER_USER=recorder
    JIBRI_XMPP_USER=jibri
    JIBRI_BREWERY_MUC=jibribrewery
    JIBRI_PENDING_TIMEOUT=90
    RESTART_POLICY=unless-stopped
    EOF

#. Then open the :file:`/etc/docker/meetingrooms/.env` and fill in:

  * ``XUC_HOST`` var with the same content as the XUC_HOST var in your **XiVO CC**
    configuration
  * ``MEETING_ROOMS_HOST_IP`` var with the meeting rooms server IP address.
    It must be the meeting rooms server IP address accessible for your LAN client.
    It must be set otherwise nothing will work correctly.
  * ``XIVO_HOST`` with the VoIP IP of the XiVO PBX
    It will be the IP towards which the Jitsi SIP gateway will register
    its SIP peer.
  * ``JWT_APP_SECRET`` with the same content as the CONFIGMGT_AUTH_SECRET var defined in your **XiVO**'s /etc/docker/xivo/custom.env file

Start the services
------------------

Start the services:

.. code-block:: bash

  meetingrooms-dcomp up -d


Configure XiVO CC
=================

On the XiVO CC, to be able to use the video service you need to configure the
``NGINX_JITSI_HOST`` variable.

#. Edit :file:`/etc/docker/compose/custom.env` file and add the ``NGINX_JITSI_HOST`` value:

   .. code-block:: ini

    NGINX_JITSI_HOST=<IP Address of the Meeting Rooms server>

#. Relaunch the services

   .. code-block:: bash

    xivocc-dcomp up -d



Configure XiVO
==============

Configure Jitsi properties in your custom env
---------------------------------------------

#. Edit :file:`/etc/docker/xivo/custom.env` file :

   .. code-block:: ini

    MEETINGROOM_AUTH_DOMAIN=<The domain serving the service, i.e. "avencall.com">
    MEETINGROOM_AUTH_APP_ID=<Value of the JWT_APP_ID defined in your jitsi .env file>

Configure the Trunk to Jitsi
----------------------------

On XiVO, go on page :menuselection:`Services --> IPBX --> Trunk management -->
SIP Protocol`, and create a SIP trunk:

* :guilabel:`Name`: xivo-jitsi
* :guilabel:`Username`: xivo-jitsi
* :guilabel:`Password`: put the value of the ``JIGASI_SIP_PASSWORD``
  variable in the :file:`/etc/docker/meetingrooms/.env`
* :guilabel:`Connection type`: Peer
* :guilabel:`IP addressing type`: Dynamic
* :guilabel:`Context`: default
* :guilabel:`Media server`: MDS Main
* :guilabel:`NAT`: Yes (force rport + comedia)


Then restart the jigasi docker on Meeting Room server::

    meetingrooms-dcomp restart jigasi

Configure XiVO Edge
===================

If you want to expose your Jitsi server externally, you need to have a XiVO Edge (See :ref:`edge_install`).

Meeting Room server configuration
---------------------------------

#. Add the following variables to :file:`/etc/docker/meetingrooms/.env` file:

   .. code-block:: bash

    # XiVO Edge vars
    XIVO_TURN_SERVER=<FILL IN>
    XIVO_TURN_SECRET=<FILL IN>
    GLOBAL_MODULES=turncredentials

#. Then fill the variable with the according values:

  * ``XIVO_TURN_SERVER`` var with the STUN/TURN server address configuration, 
    you must put the FQDN of the server with a valid certificate for the STUN/TURN server machine.
  * ``XIVO_TURN_SECRET`` var with the value of the ``TURN_SECRET`` (the secret generated during :ref:`edge_config_turnsecret`)

.. _create_meeting_room:

Create a static Meeting Room
============================

In order to create a static meeting room in XiVO (meaning available for all the users of the XIVO)
go to :menuselection:`Services --> IPBX --> IPBX Settings --> Meeting rooms`

.. figure:: images/meeting_room.png
   :scale: 100%
