.. _meetingrooms:

*************
Meeting Rooms
*************

Meeting Rooms is a new feature introduced in the Helios LTS.

It introduces Video Conference in XiVO.

**Installation and configuration are described below:**

.. toctree::
   :maxdepth: 2

   install
   configuration

**For feature description and users guide see below:**

.. contents:: :local:

.. _meetingroom_features:

Features
========

* Open video from UC Assistant, CC Agent, Switchboard
* Link to access meetingroom for external participant
* Create a static meeting room
* Create a personal meeting room from the assistant
* Join a meeting room via a phone
* Join a meeting room via incoming call

Limitations
-----------

* There must be at least one video participant for the meeting room to work.
  If only audio participants are in the conference they won't be able to speak to each other.


Not Supported Features
----------------------

(Currently these features are not supported)

* Meeting room with PIN


.. _meetingroom_usersguide:

Users' Guide
============

Join Meeting Room from Assistant
--------------------------------

To join a meeting room, you need to look for it from your Assistant applications (UC Assistant, CC Agent, Switchboard).
By typing part of its name (display name, name, number) you'll fetch your matching personal meeting rooms and the matching static meeting rooms.

Then you can click on the call action and click on the call button to join the meeting room.


Join Meeting Room for External Participants
-------------------------------------------

It's possible to share a link to people that doesn't use any XiVO assistant. It can be even a public link access (see :ref:`xivo_edge` section).

To do so, search for a meeting room in your phonebook then click on share icon. The link will be copied in your clipboard.

.. figure:: images/phonebook_meeting_room.png

Join Meeting Room from Phone
----------------------------

You can also access a Meeting Room with a phone by dialing ``**MEETING_ROOM_NUMBER`` where ``MEETING_ROOM_NUMBER`` is the Meeting Room number.
You can only access Meeting Room which are configured the XiVO PBX with a number.

Given the XiVO PBX admin user created a Meeting Room:

* :guilabel:`Name`: ProjectManagerConf
* :guilabel:`Display Name`: Project Manager Conf
* :guilabel:`Number`: 5000

Then someone can join the conference via its phone set by dialing: ``**5000``

.. _create_personal_meeting_room:

Create a personal Meeting room
------------------------------

You can create a personal meeting room from the UCAssistant menu, with the name and pincode of your choice.

.. figure:: images/personal_meeting_room.png
    :scale: 80%        


Other users can join a personal meeting room with an invitation link that can be found by the creator
of the room, when clicking on his meetingroom in the search results or favorites (share button).

Search Meeting Room
-------------------

When you search something in your Assistant (UC Assistant/CC Agent/Switchboard)
the result will contain the matching meeting room.

There is a configurable keyword that allows you to list all available rooms. By default it is ``conference``.

.. note:: For this to work, CTI Display Filter configuration must match the
   :ref:`xivo_meetingroom plugin configuration <dird-backend-xivo_meetingroom>`.
