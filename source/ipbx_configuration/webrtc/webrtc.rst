******
WebRTC
******

General notes
=============

.. note:: added in version 2016.04

XiVO comes with a WebRTC lines support, you can use in with XiVO *UC Assistant* and *Desktop Assistant*.
Before starting, please check the :ref:`webrtc_requirements`.

Current WebRTC implementation requires following configuration steps:

* configure :ref:`asterisk_http_server`,
* and create user :ref:`with one line configured for WebRTC <configure_user_with_webrtc_line>`. To have user with both SIP and WebRTC line is not supported.

.. _configure_user_with_webrtc_line:

Configuration of user with WebRTC line
======================================

1. Create user

2. Add line to user without any device

3. Edit the line created and, in the *Advanced* tab, add `webrtc=yes` options:

.. figure:: webrtc_line.png
    :scale: 100%

Fallback Configuration
======================

When the user is not connected to its WebRTC line, or disconnect from the assistant, you can route the call to a default number
as for example the user mobile number.
Update the fail option on the No Answer user tab configuration, and add an extension to the appropriate context.

.. figure:: user_fail.png
    :scale: 100%

.. _configure_webrtc_video:

Experimental video call feature
===============================

  .. important:: **Removed since 2021.08**

Manual configuration of user with WebRTC line
==============================================

For the records 

.. toctree::
    
    webrtc_manualconf

