.. _phonebook:

*********
Phonebook
*********

A global phone book can be defined in :menuselection:`Services --> IPBX --> IPBX Services -->
Phonebook`. The phone book can be used from XiVO assistants, from the phones directory look key if
the phone is compatible and are used to set the Caller ID for incoming calls.

You can add entries one by one or you can mass-import from a CSV file.

.. note:: To configure phonebook, see :ref:`directories`.

.. _meeting_room_number_in_phonebook:

Meeting room contact
====================

You can create a meeting room contact to been seen as an entry in your phonebook so that it can be searched and clickable from assistant.

To do so you need to fill at least:
* displayname of the room
* any number field with value following this format : `video:roomName` where roomName is the value you defined when you created a meeting room in XiVO webi. See :ref:`create_meeting_room`.

Mass-import contacts
====================

Go in the :menuselection:`Services --> IPBX --> IPBX Services --> Phonebook` section and move your
mouse cursor on the *+* button in the upper right corner. Select *Import a file*.

The file to be imported must be a CSV file, with a pipe character *|* as field delimiter. The file
must be encoded in UTF-8 (without an initial `BOM`_).

Mandatory headers are :

* title (possible values : "mr", "mrs", "ms")
* displayname

Optional headers are :

* firstname
* lastname
* society
* mobilenumber [#numeric]_
* email
* url
* description
* officenumber [#numeric]_
* faxnumber [#numeric]_
* officeaddress1
* officeaddress2
* officecity
* officestate
* officezipcode
* officecountry [#country]_
* homenumber [#numeric]_
* homeaddress1
* homeaddress2
* homecity
* homestate
* homezipcode
* homecountry [#country]_
* othernumber [#numeric]_
* otheraddress1
* otheraddress2
* othercity
* otherstate
* otherzipcode
* othercountry [#country]_


.. [#numeric] These fields must contain either numeric characters, or a video meeting number starting with "video:". For example you can write : "video: my wednesday meeting". The following characters are not allowed in the meeting room name : ? & ' " % : #  
.. [#country] These fields must contain ISO country codes. The complete list is described `here`_.
.. _here: http://www.iso.org/iso/country_codes/iso_3166_code_lists/country_names_and_code_elements.htm
.. _BOM: http://www.unicode.org/faq/utf_bom.html#BOM
