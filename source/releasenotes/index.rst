.. _xivosolutions_release:

*************
Release Notes
*************

.. _helios_release:

Helios (2021.15)
================

Below is a list of *New Features* and *Behavior Changes* compared to the previous LTS version, Gaia (2021.07).

New Features
------------

**Meeting Rooms**
* Add the video conferencing with window sharing on top of all XiVO assistants see :ref:`meetingrooms`.

**Assistants**

* *Common features*

  * Added message on the browser and the desktop application when the window is too small to properly use the application
  * Added the possibility to change the fallback lang for applications - see :ref:`webassistant_lang_fallback`
* *Desktop Application*

  * The Desktop assistant can be resized but still has a minimal size.
    It no longer automatically resize when you change the screen it is displayed on.
* *UC Assistant*

  * Phone status for contacts coming from a different source than internal XiVO Users is displayed if its number matches a XiVO user number in the Favorites view.
    It was already the case for Search results view but it wasn't working in the Favorites view if you selected as favorite the contact that was not coming from internal XiVO users source.
  * It is now possible to search for static and personal meeting rooms

**API**

* Added new meeting rooms API, see :ref:`configmgt_api_meeting_rooms`
* Added a new icon (group of users) on the contact line if they have a meeting room number
* In the webi, we can now write a number for a meeting room in the phonebook see :ref:`meeting_room_number_in_phonebook`

**Audio Conferences**

* When an organizer leaves a conference, the participant states are set to their default state.
  All muted or deaf participants are not muted or deaf anymore.

**XiVO PBX**

* New menu *Meeting rooms* to create meeting room

**System**

* Upgrade electron to 13.1.0
* Upgrade nginx to 1.21.0
* Ugrade ELK stack from 7.10 to 7.14

Behavior Changes
----------------

**Assistants**

* *Desktop Application*

  * The Desktop assistant no longer has a fixed size. It can be resized but still has a minimal size.
    It no longer automatically resize when you change the screen it is displayed on.
* *UC Assistant*

  * Video experimental feature is removed. This feature is replaced by :ref:`meetingrooms` features.
  * Phone status for contacts coming from a different source than internal XiVO Users is displayed if its number matches a XiVO user number in the Favorites view.
    It was already the case for Search results view but it wasn't working in the Favorites view if you selected as favorite the contact that was not coming from internal XiVO users source.


**Audio Conferences**

* When an organizer leaves a conference, the participant states are set to their default state.
  All muted or deaf participants are not muted or deaf anymore.

**Reporting**

* Elasticsearch JVM is by default started with 1.5Gb of RAM. It can be changed following :ref:`reporting_es_memory` section.

**API**

* *Recording Server*: Previously (before Gaia.10) you had to add an Administrator profile to the "xuc technical" user via the Config Mgt profile management to be able to use the Recording Server API with the token.
  This is no longer needed.
  To use the Recording Server API you now only need to use the token - see Authentication section of :ref:`recording_api`.
* XUC :ref:`web_socket_api` now integrates a throttling mechanism to prevent flooding. If you exceed more than 15 request messages in 30 seconds (with a burst of 25), your messages will be throttled and you will receive an error `{msgType: "Error", ctiMessage: {Error: "Maximum throttle throughput exceeded."}}`

.. _helios_release_deprecations:

Deprecations
------------

This release deprecates:

* `LTS Borealis (2018.16) <https://documentation.xivo.solutions/en/2018.16/>`_: after 3 years of support this version is no longer supported.
  No bug fixes, no security update will be provided for this release.
* Video experimental feature is removed.
* Deprecated API ``/ctichannel`` to access CTI WS was completely removed (it was deprecated since LTS XiVO Five) - see :ref:`user_api_login` for current way to open CTI WS.


Upgrade
-------

.. _upgrade_lts_manual_steps:

**Manual steps for LTS upgrade**

.. warning:: **Don't forget** to read carefully the specific steps to upgrade from another LTS version

.. toctree::
   :maxdepth: 1

   upgrade_from_five_to_polaris
   upgrade_from_polaris_to_aldebaran
   upgrade_from_aldebaran_to_borealis
   upgrade_from_borealis_to_callisto
   upgrade_from_callisto_to_deneb
   upgrade_from_deneb_to_electra
   upgrade_from_electra_to_freya
   upgrade_from_freya_to_gaia
   upgrade_from_gaia_to_helios


**Generic upgrade procedure**

Then, follow the generic upgrade procedures:

* :ref:`XiVO PBX upgrade procedure <upgrade>`
* :ref:`XiVO CC upgrade procedure <upgrade_cc>`
* :ref:`XDS upgrade procedure <upgrade_xds>`

Helios Bugfixes Versions
========================

Components version table
------------------------

Table listing the current version of the components.

+----------------------+----------------+
| Component            | current ver.   |
+======================+================+
| **XiVO**                              |
+----------------------+----------------+
| XiVO PBX             | 2021.15.00     |
+----------------------+----------------+
| config_mgt           | 2021.15.00     |
+----------------------+----------------+
| db                   | 2021.15.00     |
+----------------------+----------------+
| outcall              | 2021.15.00     |
+----------------------+----------------+
| db_replic            | 2021.15.00     |
+----------------------+----------------+
| nginx                | 2021.15.00     |
+----------------------+----------------+
| webi                 | 2021.15.00     |
+----------------------+----------------+
| switchboard_reports  | 2021.15.00     |
+----------------------+----------------+
| **XiVO CC**                           |
+----------------------+----------------+
| elasticsearch        | 7.14.0         |
+----------------------+----------------+
| kibana               | 7.14.0         |
+----------------------+----------------+
| logstash             | 2021.15.00     |
+----------------------+----------------+
| mattermost           | 2021.15.00     |
+----------------------+----------------+
| nginx                | 2021.15.00     |
+----------------------+----------------+
| pack-reporting       | 2021.15.00     |
+----------------------+----------------+
| pgxivocc             | 2021.15.00     |
+----------------------+----------------+
| recording-rsync      | 1.0            |
+----------------------+----------------+
| recording-server     | 2021.15.00     |
+----------------------+----------------+
| spagobi              | 2021.15.00     |
+----------------------+----------------+
| xivo-full-stats      | 2021.15.00     |
+----------------------+----------------+
| xuc                  | 2021.15.00     |
+----------------------+----------------+
| xucmgt               | 2021.15.00     |
+----------------------+----------------+
| **Edge**                              |
+----------------------+----------------+
| nginx                | 2021.15.00     |
+----------------------+----------------+
| kamailio             | 2021.15.00     |
+----------------------+----------------+
| coturn               | 2021.15.00     |
+----------------------+----------------+
| **Meeting Rooms**                     |
+----------------------+----------------+
| meetingroom          | 2021.15.00     |
+----------------------+----------------+
| web-jitsi            | 2021.15.00     |
+----------------------+----------------+
| jicofo-jitsi         | 2021.15.00     |
+----------------------+----------------+
| prosody-jitsi        | 2021.15.00     |
+----------------------+----------------+
| jvb-jitsi            | 2021.15.00     |
+----------------------+----------------+
| jigasi-jitsi         | 2021.15.00     |
+----------------------+----------------+

Helios Intermediate Versions
============================

.. toctree::
   :maxdepth: 2

   helios_iv
