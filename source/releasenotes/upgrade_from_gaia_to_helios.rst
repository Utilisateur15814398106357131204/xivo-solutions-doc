**********************
Upgrade Gaia to Helios
**********************

Before Upgrade
==============

On XiVO PBX
-----------


On XiVO CC/UC
-------------

* Reporting: During upgrade all **Kibana configuration** (including the dashboard) will be lost (it is stored in *elasticsearch* container).
  You **MUST** :ref:`backup Kibana configuration <xivocc_backup_kibana>` before the upgrade.


On MDS
------


After Upgrade
=============

On XiVO PBX
-----------


On XiVO CC/UC
-------------

* Reporting:

  * :ref:`Restore Kibana configuration <xivocc_restore_kibana>`.
  * The last 7 days of data will be re-replicated to Elasticsearch, see :ref:`totem_data_flow`.
    It may take some time if you have a huge amount of calls per week (more than 1 hour if you have 2 million of queue_log per week).

* **XDS installation only**: you need to update the nginx configuration for WebRTC on MDS if you had already followed the :ref:`xds_webrtc-configuration`

  #. On your XiVO CC hosting the nginx server, edit the file :file:`/etc/docker/nginx/sip_proxy/sip_proxy.conf`
     (if this file doesn't exist on your server you can skip this)
  #. Inside the location,

     #. remove all the lines containing the parameters:

        * ``proxy_http_version``,
        * ``proxy_set_header``,
        * ``proxy_{connect,read,send}_timeout``,
        * and ``proxy_buffering off``
     #. and replace it by the two following includes::

         include /etc/nginx/xivo/proxy-ws_params;
         include /etc/nginx/xivo/proxy_params;
     #. be sure to keep the ``keepalive_timeout 180s`` parameter.
  #. Below is an example before/after with mds1 as MDS_NAME:

   +------------------------------------------+--------------------------------------------+
   | Before                                   | After                                      |
   +==========================================+============================================+
   | .. code-block:: ini                      | .. code-block:: ini                        |
   |  :emphasize-lines: 5-12                  |  :emphasize-lines: 5-6                     |
   |                                          |                                            |
   |  location /wssip-mds1 {                  |  location /wssip-mds1 {                    |
   |   auth_request /validatetoken;           |   auth_request /validatetoken;             |
   |                                          |                                            |
   |   proxy_pass http://10.32.0.201:5039/ws; |   proxy_pass http://10.32.0.201:5039/ws;   |
   |   proxy_http_version 1.1;                |   include /etc/nginx/xivo/proxy-ws_params; |
   |   proxy_set_header Upgrade $http_upgrade;|   include /etc/nginx/xivo/proxy_params;    |
   |   proxy_set_header Connection "upgrade"; |                                            |
   |   proxy_set_header Host $host;           |                                            |
   |   proxy_buffering off;                   |                                            |
   |   proxy_connect_timeout 1m;              |                                            |
   |   proxy_read_timeout 5m;                 |                                            |
   |   proxy_send_timeout 5m;                 |                                            |
   |   keepalive_timeout 180s;                |   keepalive_timeout 180s;                  |
   |  }                                       |  }                                         |
   +------------------------------------------+--------------------------------------------+



On MDS
------
