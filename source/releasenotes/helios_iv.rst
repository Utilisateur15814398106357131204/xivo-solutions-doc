*********************************
XiVO Helios Intermediate Versions
*********************************

2021.14
-------

Consult the `2021.14 Roadmap <https://projects.xivo.solutions/versions/274>`_.

Components updated: **config-mgt**, **logstash**, **play-authentication**, **recording-server**, **xivo-agentd-client**, **xivo-agid**, **xivo-confd-client**, **xivo-config**, **xivo-dao**, **xivo-db**, **xivo-dird-client**, **xivo-lib-python**, **xivo-meetingrooms**, **xivo-prosody-jitsi**, **xivo-provd-client**, **xivo-web-interface**, **xivocc-installer**, **xucmgt**, **xucserver**

**Config mgt**

* `#4328 <https://projects.xivo.solutions/issues/4328>`_ - Update play-auth dependency in configmgt

**Directory**

* `#4249 <https://projects.xivo.solutions/issues/4249>`_ - Dird Improvement - Create database structure for contacts

**Recording**

* `#4281 <https://projects.xivo.solutions/issues/4281>`_ - Token authentication to recording server api retrieves wrong user

**Visioconf**

* `#4270 <https://projects.xivo.solutions/issues/4270>`_ - Meeting Room - Play announcement to audio only caller while joining the room
* `#4284 <https://projects.xivo.solutions/issues/4284>`_ - Meeting Rooms - Add support for meeting room with special character for audio only access
* `#4285 <https://projects.xivo.solutions/issues/4285>`_ - Meeting Room - Be able to join the meeting room audio only from an MDS
* `#4351 <https://projects.xivo.solutions/issues/4351>`_ - As a user I want to see that user B is online when in a meeting room call

**Web Assistant**

* `#4357 <https://projects.xivo.solutions/issues/4357>`_ - Cannot close the Third Party Application
* `#4360 <https://projects.xivo.solutions/issues/4360>`_ - Fix xucmgt build on master branch

**XiVO PBX**

* `#4296 <https://projects.xivo.solutions/issues/4296>`_ - [Python 2 to 3] make xivo-confd-client compatible with both python 2 and 3 
* `#4307 <https://projects.xivo.solutions/issues/4307>`_ - [Python 2 to 3] make xivo-agentd-client compatible with both python 2 and 3 
* `#4308 <https://projects.xivo.solutions/issues/4308>`_ - [Python 2 to 3] make xivo-dird-client compatible with both python 2 and 3 
* `#4317 <https://projects.xivo.solutions/issues/4317>`_ - [Python 2 to 3] migrate xivo-agid to python3
* `#4341 <https://projects.xivo.solutions/issues/4341>`_ - Drop-Down Menu not displayed alongside Queues
* `#4356 <https://projects.xivo.solutions/issues/4356>`_ - [Python 2 to 3] Xivo-agid modifications for xivo-lib-python
* `#4361 <https://projects.xivo.solutions/issues/4361>`_ - Meeting Room - DB - Add personal meeting room

**XiVOCC Infra**

* `#4183 <https://projects.xivo.solutions/issues/4183>`_ - Update ELK stack to 7.14

  .. important:: **Behavior change** Elasticsearch indexes are removed and reprocessed with last 7 days of data. Kibana configuration is also cleared and need to be reimported.
    
    Take care to export your Kibana configuration if you have custom reports !


**edge**

* `#4031 <https://projects.xivo.solutions/issues/4031>`_ - Xivo-Edge - Allow NATed DMZ configuration


2021.13
=======

Consult the `2021.13 Roadmap <https://projects.xivo.solutions/versions/273>`_.

Components updated: **config-mgt**, **edge-nginx**, **xivo-confd**, **xivo-dao**, **xivo-db**, **xivo-edge**, **xivo-install-script**, **xivo-lib-python**, **xivo-web-interface**, **xucmgt**, **xucserver**

**Config mgt**

* `#4345 <https://projects.xivo.solutions/issues/4345>`_ - Meeting Room - Add room name in response header and set cookie on redirect

**Visioconf**

* `#4170 <https://projects.xivo.solutions/issues/4170>`_ - As a Xivo user I want a to be able to copy the external link information from the directory results
* `#4277 <https://projects.xivo.solutions/issues/4277>`_ - Meeting Rooms - As a user I want to share the link for static meeting room
* `#4278 <https://projects.xivo.solutions/issues/4278>`_ - Meeting Rooms - External link generation for static meeting room without password
* `#4298 <https://projects.xivo.solutions/issues/4298>`_ - Video service - uuid meeting room
* `#4299 <https://projects.xivo.solutions/issues/4299>`_ - Video Service - Add route in edge-nginx to jitsi and validate token
* `#4301 <https://projects.xivo.solutions/issues/4301>`_ - Video conference access token generation
* `#4302 <https://projects.xivo.solutions/issues/4302>`_ - Video conference access token validation API
* `#4322 <https://projects.xivo.solutions/issues/4322>`_ - Prepare certilience environment for meeting room audit
* `#4346 <https://projects.xivo.solutions/issues/4346>`_ - Access room name from UC assistant with Edge
* `#4347 <https://projects.xivo.solutions/issues/4347>`_ - Given I have a Meeting rom share link I should not be able to open another room
* `#4349 <https://projects.xivo.solutions/issues/4349>`_ - Configmgt cookies set after validating meetingRoom token should not the HttpOnly

**WebRTC**

* `#4332 <https://projects.xivo.solutions/issues/4332>`_ - "Could not initialize call" error is misleading when desktop app cannot access mic

**XiVO PBX**

* `#4331 <https://projects.xivo.solutions/issues/4331>`_ - In meetingroom table, pin number and room number columns are integers instead of string
* `#4335 <https://projects.xivo.solutions/issues/4335>`_ - confd -https endpoint for swagger api descriptor does not work
* `#4337 <https://projects.xivo.solutions/issues/4337>`_ - Cannot use forward func keys on phones
* `#4339 <https://projects.xivo.solutions/issues/4339>`_ - Confd logging error when restarting services
* `#4342 <https://projects.xivo.solutions/issues/4342>`_ - Debian 11 new stable repos breaks xivo-installer


2021.12
=======

Consult the `2021.12 Roadmap <https://projects.xivo.solutions/versions/270>`_.

Components updated: **config-mgt**, **edge-nginx**, **xivo-amid**, **xivo-auth-client**, **xivo-bus**, **xivo-confd**, **xivo-jicofo-jitsi**, **xivo-jigasi-jitsi**, **xivo-jvb-jitsi**, **xivo-lib-python**, **xivo-lib-rest-client**, **xivo-meetingrooms**, **xivo-prosody-jitsi**, **xivo-provd-client**, **xivo-test-helpers**, **xivo-web-jitsi**, **xucmgt**

**CCAgent**

* `#4323 <https://projects.xivo.solutions/issues/4323>`_ - CCAgent - Remember the login after each disconnection

**Desktop Assistant**

* `#4231 <https://projects.xivo.solutions/issues/4231>`_ - Msi build is failing every time even if set in success in Jenkins
* `#4232 <https://projects.xivo.solutions/issues/4232>`_ - The desktop assistant is too wide when unmaximize on Windows

**Directory**

* `#4247 <https://projects.xivo.solutions/issues/4247>`_ - Dird Improvement - Add API to configmgt to get Xivo Users with contact informations

**Visioconf**

* `#4162 <https://projects.xivo.solutions/issues/4162>`_ - Meeting Rooms - Jitsi and Plan-B/Unified plan
* `#4171 <https://projects.xivo.solutions/issues/4171>`_ - Meeting Rooms - Study how to integrate and authenticate with Edge TURN
* `#4218 <https://projects.xivo.solutions/issues/4218>`_ - Meeting rooms : Create a meeting room from the personnal contacts in the uc assistant
* `#4230 <https://projects.xivo.solutions/issues/4230>`_ - Name on Jitsi is not changed when relogging a user
* `#4300 <https://projects.xivo.solutions/issues/4300>`_ - Video service  - open a jitsi room from a UUID 

**Web Assistant**

* `#4276 <https://projects.xivo.solutions/issues/4276>`_ - [C] - As a phone user with an ongoing call, when I quit/refresh the Web Assistant it hangup my ongoing call

**XiVO PBX**

* `#4246 <https://projects.xivo.solutions/issues/4246>`_ - Convert xivo-bus to python2/3
* `#4260 <https://projects.xivo.solutions/issues/4260>`_ - [Python 2 to 3] make xivo-lib-rest-client compatible with both python 2 and 3 
* `#4262 <https://projects.xivo.solutions/issues/4262>`_ - [Python 2 to 3] make xivo-provd-client compatible with both python 2 and 3 
* `#4271 <https://projects.xivo.solutions/issues/4271>`_ - [Python 2 to 3] make xivo-confd compatible with both python 2 and 3 
* `#4327 <https://projects.xivo.solutions/issues/4327>`_ - sip.conf generation too slow for edge endpoints - helios
* `#4333 <https://projects.xivo.solutions/issues/4333>`_ - xivo-amid need fix because of python 2 deprecation

**XiVOCC Infra**

* `#4188 <https://projects.xivo.solutions/issues/4188>`_ - You can have multiple jitsi conference open in the same time
* `#4283 <https://projects.xivo.solutions/issues/4283>`_ - Update python 3 version for remaining projects

2021.11
=======

Consult the `2021.11 Roadmap <https://projects.xivo.solutions/versions/267>`_.

Components updated: **config-mgt**, **sipml5-xivo-mirror**, **xivo-agid**, **xivo-confgend**, **xivo-config**, **xivo-dao**, **xivo-db**, **xivo-install-script**, **xivo-meetingrooms**, **xivo-web-interface**, **xivo-web-jitsi**, **xivocc-installer**, **xivoxc-nginx**, **xucmgt**, **xucserver**

**Visioconf**

* `#4124 <https://projects.xivo.solutions/issues/4124>`_ - Meeting Room - Being able to join the jitsi room audio only
* `#4130 <https://projects.xivo.solutions/issues/4130>`_ - Meeting Rooms - I want to be able to install Jitsi services on a separate server
* `#4195 <https://projects.xivo.solutions/issues/4195>`_ - As an admin I can associate an incoming call to a meeting room

**Web Assistant**

* `#4272 <https://projects.xivo.solutions/issues/4272>`_ - UC assistant and CCagent search bar does not work anymore

**WebRTC**

* `#4210 <https://projects.xivo.solutions/issues/4210>`_ - WebRTC Conferences are not working with Unified Plan
* `#4245 <https://projects.xivo.solutions/issues/4245>`_ - Webrtc audio gauge are broken by switching from one call to another

**XUC Server**

* `#4186 <https://projects.xivo.solutions/issues/4186>`_ - [Doc] Improve Relase Notes Warning about deprecatd  xuc api

**XiVO PBX**

* `#4261 <https://projects.xivo.solutions/issues/4261>`_ - [Python 2 to 3] make xivo-config compatible with both python 2 and 3

**XiVOCC Infra**

* `#4235 <https://projects.xivo.solutions/issues/4235>`_ - Wrong Debian version check for older version for XiVOCC and MDS install script
* `#4257 <https://projects.xivo.solutions/issues/4257>`_ - Jenkins job update : build python projects on jenkins within docker

**edge**

* `#4234 <https://projects.xivo.solutions/issues/4234>`_ - [Doc] Fill Edge server requirements in doc


2021.10
=======

Consult the `2021.10 Roadmap <https://projects.xivo.solutions/versions/255>`_.

Components updated: **xivo-agid**, **xivo-confgend**, **xivo-config**, **xivo-dao**, **xivo-lib-python**, **xivo-lib-rest-client**, **xivo-sysconfd**, **xivo-test-helpers**, **xivo-web-interface**, **xivo-web-jitsi**, **xucmgt**, **xucserver**

**Desktop Assistant**

* `#4144 <https://projects.xivo.solutions/issues/4144>`_ - Hangup a Jitsi call in Desktop application always minify the window
* `#4158 <https://projects.xivo.solutions/issues/4158>`_ - Electron : button to minimize the ccagent doesn't work

**Visioconf**

* `#4167 <https://projects.xivo.solutions/issues/4167>`_ - Disable Jitsi Options
* `#4198 <https://projects.xivo.solutions/issues/4198>`_ - As an admin I can add a meeting room entry in the xivo phonebook
* `#4199 <https://projects.xivo.solutions/issues/4199>`_ - Meeting Room - Add jigasi to the jitsi deployment
* `#4224 <https://projects.xivo.solutions/issues/4224>`_ - Xivo-config : adapt build to xivo-lib-python changes

**Web Assistant**

* `#4042 <https://projects.xivo.solutions/issues/4042>`_ - Being able to hide the assistant during video call
* `#4052 <https://projects.xivo.solutions/issues/4052>`_ - As a user when I call a conference I want to see my name on Jitsi
* `#4143 <https://projects.xivo.solutions/issues/4143>`_ - User presence status sometimes missing in favorites

  .. important:: **Behavior change** Phone status for contacts coming from a different source than internal XiVO Users is displayed if its number matches a XiVO user number in the Favorites view.
    It was already the case for Search results view but it wasn't working in the Favorites view if you selected as favorite the contact that was not coming from internal XiVO users source.

* `#4184 <https://projects.xivo.solutions/issues/4184>`_ - Personal contact window cannot be opened after beeing closed

**WebRTC**

* `#3435 <https://projects.xivo.solutions/issues/3435>`_ - As a webRTC user I can manage two calls

**XUC Server**

* `#4054 <https://projects.xivo.solutions/issues/4054>`_ - First connection after recreating the line of a WebRTC user not possible
* `#4118 <https://projects.xivo.solutions/issues/4118>`_ - xucserver refreshes Line/User config from rabbitmq only

**XiVO PBX**

* `#4215 <https://projects.xivo.solutions/issues/4215>`_ - Corrective Changes in python3
* `#4213 <https://projects.xivo.solutions/issues/4213>`_ - Make xivo-dao cross compatible py2/py3
* `#4214 <https://projects.xivo.solutions/issues/4214>`_ - Make xivo-lib-python cross compatible py2/py3


2021.09
=======

Consult the `2021.09 Roadmap <https://projects.xivo.solutions/versions/254>`_.

Components updated: **config-mgt**, **edge-nginx**, **recording-server**, **xivo-dao**, **xivo-db**, **xivo-provd-plugins**, **xivo-web-interface**, **xivo-webi-nginx**, **xivocc-installer**, **xivoxc-nginx**, **xucmgt**, **xucserver**

**CCAgent**

* `#4110 <https://projects.xivo.solutions/issues/4110>`_ - Resize message should not appear for CCAgent when bar is minified
* `#4135 <https://projects.xivo.solutions/issues/4135>`_ - [C] - Agent custom pause reason is not shown in CCAgent and in CCManager (Port to Helios)
* `#4179 <https://projects.xivo.solutions/issues/4179>`_ - Missing translation for CC Agent for UNABLE_TO_START_WEBRTC

**Config mgt**

* `#4128 <https://projects.xivo.solutions/issues/4128>`_ - Configmgt - Meeting Rooms API - List All/CRUD with filtering
* `#4164 <https://projects.xivo.solutions/issues/4164>`_ - Add swagger and ui to configmgt

**Desktop Assistant**

* `#4012 <https://projects.xivo.solutions/issues/4012>`_ - [C] - Be able to configure the lang fallback
* `#4044 <https://projects.xivo.solutions/issues/4044>`_ - As a user of the desktop assistant I want to be able to define the size of the window
* `#4045 <https://projects.xivo.solutions/issues/4045>`_ - The desktop assistant should react to the start and end of a video conference
* `#4057 <https://projects.xivo.solutions/issues/4057>`_ - Desktop assistant msi package is missing icon
* `#4102 <https://projects.xivo.solutions/issues/4102>`_ - Update desktop assistant to electron 13
* `#4178 <https://projects.xivo.solutions/issues/4178>`_ - UC Assistant - Fix Call management window Close button translation in German

**Recording**

* `#3543 <https://projects.xivo.solutions/issues/3543>`_ - Customer call history duplicated
* `#4049 <https://projects.xivo.solutions/issues/4049>`_ - Recording Server Auth for user xuc still possible with old unsecure password

**Switchboard**

* `#3991 <https://projects.xivo.solutions/issues/3991>`_ - [C] - POPC - Park call : caller's number takes the agent's number

**Visioconf**

* `#4161 <https://projects.xivo.solutions/issues/4161>`_ - Meeting Rooms - We can be more than 2 users in the same conf

**Web Assistant**

* `#4041 <https://projects.xivo.solutions/issues/4041>`_ - Add the use of Jitsi server in the web assistant
* `#4084 <https://projects.xivo.solutions/issues/4084>`_ - Remove experimental video feature
* `#4139 <https://projects.xivo.solutions/issues/4139>`_ - Enhance message display when web application window is too small
* `#4154 <https://projects.xivo.solutions/issues/4154>`_ - Some modals are too large

**XUC Server**

* `#3704 <https://projects.xivo.solutions/issues/3704>`_ - [C] Automatic conference actions when organizer leaves conference

  .. important:: **Behavior change** When an organizer leaves a conference, the participant states are set to their default state. All muted or deaf participants are not muted or deaf anymore.

* `#4166 <https://projects.xivo.solutions/issues/4166>`_ - [C] - Fix Audience when using multiple client id with OpenID (port to Helios)

**XiVO PBX**

* `#4127 <https://projects.xivo.solutions/issues/4127>`_ - Webi - As a Webi Admin I can list, CRUD, filter Meeting room
* `#4132 <https://projects.xivo.solutions/issues/4132>`_ - Database - Meeting Rooms - Add table

**XiVO Provisioning**

* `#4056 <https://projects.xivo.solutions/issues/4056>`_ - Provisioning - Add ATA 192 to plugin xivo-cisco-spa100-xxx

**edge**

* `#4140 <https://projects.xivo.solutions/issues/4140>`_ - [Security] Update Nginx - 1.21.0 - DNS Resolver Vulnerability - CVE-2021-23017

  .. important:: **Behavior change** Nginx version was upgraded from 1.20.0 to 1.21.0 (in XiVO, XiVOCC and Edge project)

* `#4151 <https://projects.xivo.solutions/issues/4151>`_ - Edge - SSO (OpenID) does not work via Edge server


2021.08
=======

Consult the `2021.08 Roadmap <https://projects.xivo.solutions/versions/251>`_.

Components updated: **edge-kamailio**, **edge-nginx**, **recording-server**, **sipml5-xivo-mirror**, **xivo-config**, **xivo-full-stats**, **xivo-switchboard-reports**, **xivo-webi-nginx**, **xivoxc-nginx**, **xucmgt**, **xucserver**

**CCAgent**

* `#4080 <https://projects.xivo.solutions/issues/4080>`_ - Switchboard (CC Agent) history is wrong for call received from/emitted towards MDS user

**Desktop Assistant**

* `#4093 <https://projects.xivo.solutions/issues/4093>`_ - [C] - Close to tray cause application re-opening errors

  .. important:: **Behavior change** Closing the Desktop Application from the Taskbar (Right-Clic -> Close Window) will now **Quit** the application.
    Beforehand the application would have been minimized in the Systray (if the option *Minimize to tray* option was checked) but then it couldn't be re-opened from the Systray.
    Note that if you have an ongoing call you'll be warned before the application is closed.


**Security**

* `#3999 <https://projects.xivo.solutions/issues/3999>`_ - [Security] message rate limiting on xuc websocket
* `#4095 <https://projects.xivo.solutions/issues/4095>`_ - [Security] Update nginx to version 1.20.0
* `#4100 <https://projects.xivo.solutions/issues/4100>`_ - [Security] Update kamailio to version 5.5.0

**Switchboard**

* `#4076 <https://projects.xivo.solutions/issues/4076>`_ - Switchboard report doesn't work

**Web Assistant**

* `#4040 <https://projects.xivo.solutions/issues/4040>`_ - Being able to install Jitsi server in docker
* `#4043 <https://projects.xivo.solutions/issues/4043>`_ - We should have a minimal size for the web assistant
* `#4051 <https://projects.xivo.solutions/issues/4051>`_ - Personnalize our image of Jitsi
* `#4096 <https://projects.xivo.solutions/issues/4096>`_ - User is not relogged automatically if an error occurs

**WebRTC**

* `#2379 <https://projects.xivo.solutions/issues/2379>`_ - Support WebRTC SDP Unified plan

**XUC Server**

* `#4079 <https://projects.xivo.solutions/issues/4079>`_ - When changing FWD destination in UC, previous FK is not updated on phone

**edge**

* `#4103 <https://projects.xivo.solutions/issues/4103>`_ - Edge - Web proxy - Allow desktop assistant install and update path
* `#4105 <https://projects.xivo.solutions/issues/4105>`_ - Edge proxy - Wrong include
