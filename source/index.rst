.. XiVO-doc Documentation master file.

********************************************************
XiVO Solutions |version| Documentation (Helios Edition)
********************************************************

.. important:: **What's new in this version ?**

   * Video conferencing with window sharing on top of all XiVO assistants see :ref:`meetingrooms`.
   * Public conference sharing link with XiVO-Edge configuration.
   * Desktop assistant enhancements. It can be resized, ability to define lang fallback and warning message if window is too small.
   * Directory improvements for more flexible search

   See :ref:`helios_release` page for the complete list of **New Features** and **Behavior Changes**.

   **Deprecations**

   * End of Support for LTS Boréalis (2018.16).

   See :ref:`helios_release_deprecations`.


.. figure:: logo_xivo.png
   :scale: 70%

XiVO_ solutions developed by Wisper_ group is a suite of PBX applications based on several free existing components including Asterisk_
and our own developments. This powerful and scalable solution offers a set of features for corporate telephony and call centers to power their business.

You may also have a look at our `development blog <http://xivo-solutions-blog.gitlab.io/>`_ for technical news about the solution

.. _Asterisk: http://www.asterisk.org/
.. _Wisper: https://wisper.io/
.. _XiVO: https://xivo.solutions/


.. toctree::
   :maxdepth: 2

   introduction/introduction
   getting_started/getting_started
   installation/index
   administrator/index
   ipbx_configuration/administration
   meetingrooms/index
   contact_center/contact_center
   edge/index
   Centralized User Management <centralized_user_management/index>
   usersguide/index
   Devices <https://documentation.xivo.solutions/projects/devices/en/latest/>
   api_sdk/api_sdk
   contributing/contributing
   releasenotes/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
