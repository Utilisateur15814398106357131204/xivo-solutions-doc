.. _edge_architecture:

*****************
Edge Architecture
*****************


Overall Architecture
====================


.. figure:: images/edge_archi_highlevelview.png
   :alt: edge_archi_highlevelview


As you can see on the schema above the Edge Solution is to be put inside one's company
DMZ.

From the external world only the Edge Solution will be seen.
Only HTTPS and TURN flows should be authorized from the external - see :ref:`edge_archi_network` for more details.


The Edge Solution is composed of three components:

* a Web Proxy
* a SIP Proxy
* and a TURN Server

These components can be installed:

* on 3 separate servers - see :ref:`edge_install_3servers`
* or on 1 server - see :ref:`edge_install_1server`


.. _edge_archi_network:

Network Flows
=============


From the outside (WAN to DMZ)
-----------------------------

.. figure:: images/edge_archi_networkflows_external.png
   :alt: network_flows_external_to_dmz

+-----------+--------------------+--------------------+-------------------------------------------------------+
| From      | To                 | Ports to open      | Usage                                                 |
+===========+====================+====================+=======================================================+
| Outside   | Web Proxy          | - TCP/443          | UC Application from External Users                    |
|           |                    |                    |                                                       |
| ``Any``   | ``DMZ_IP1``        |                    |                                                       |
+-----------+--------------------+--------------------+-------------------------------------------------------+
| Outside   | TURN Server        | - UDP/3478         | STUN/TURN via UDP, TCP                                |
|           |                    | - TCP/3478         |                                                       |
| ``Any``   | ``DMZ_IP3``        |                    |                                                       |
+-----------+--------------------+--------------------+-------------------------------------------------------+
| Outside   | TURN Server        | - UDP/30000-39999  | RTP Flow between remote WebRTC client and TURN Server |
|           |                    | - TCP/30000-39999  |                                                       |
| ``Any``   | ``DMZ_IP3``        |                    |                                                       |
+-----------+--------------------+--------------------+-------------------------------------------------------+


Internally (DMZ to LAN)
-----------------------

.. figure:: images/edge_archi_networkflows_internal.png
   :alt: network_flows_dmz_to_internal

+-------------+----------------------------+------------------------------------+---------------------------------------------------------------+
| From/To     | To/From                    | Ports to open                      | Usage                                                         |
+=============+============================+====================================+===============================================================+
| Web Proxy   | XiVO CC                    | - TCP/443                          | Proxified UC application from Edge towards CC/UC Server       |
|             |                            |                                    |                                                               |
| ``DMZ_IP1`` | ``LAN_IP1``                |                                    |                                                               |
+-------------+----------------------------+------------------------------------+---------------------------------------------------------------+
| Web Proxy   | UC Clients                 | - TCP/443                          | UC Application for Internal Users                             |
|             |                            |                                    |                                                               |
| ``DMZ_IP1`` | ``LAN*``                   |                                    |                                                               |
+-------------+----------------------------+------------------------------------+---------------------------------------------------------------+
| TURN Server | Mediaservers (inc. Main)   | - UDP/3478                         | STUN/TURN request between Asterisk and TURN Server            |
|             |                            | - TCP/3478                         |                                                               |
|             |                            |                                    |                                                               |
| ``DMZ_IP3`` | ``LAN_IP2``, ``LAN_IP3``   |                                    |                                                               |
+-------------+----------------------------+------------------------------------+---------------------------------------------------------------+
| TURN Server | Mediaservers (inc. Main)   | - UDP/30000-39999                  | RTP Flow between Asterisk and TURN Server (when using TURN)   |
|             |                            | - TCP/30000-39999                  |                                                               |
|             |                            |                                    |                                                               |
| ``DMZ_IP3`` | ``LAN_IP2``, ``LAN_IP3``   |                                    |                                                               |
+-------------+----------------------------+------------------------------------+---------------------------------------------------------------+
| SIP Proxy   | Mediaservers (inc. main)   | - UDP/5060 Both ways               | SIP flow between Asterisk(s) and SIP Proxy                    |
|             |                            |                                    |                                                               |
|             |                            |                                    |                                                               |
| ``DMZ_IP2`` | ``LAN_IP2``, ``LAN_IP3``   |                                    |                                                               |
+-------------+----------------------------+------------------------------------+---------------------------------------------------------------+

Access for internal clients towards Edge servers (**LAN to DMZ**):

+-------------+----------------------------+------------------------------------+---------------------------------------------------------------+
| From        | To                         | Ports to open                      | Usage                                                         |
+=============+============================+====================================+===============================================================+
| UC Clients  | Web Proxy                  | - TCP/443                          | UC Application for Internal Users                             |
|             |                            |                                    |                                                               |
| ``Any LAN`` | ``DMZ_IP1``                |                                    |                                                               |
+-------------+----------------------------+------------------------------------+---------------------------------------------------------------+
| UC Clients  | TURN Server                | - UDP/3478                         | STUN/TURN request between UC Clients and TURN Server          |
|             |                            | - TCP/3478                         |                                                               |
| ``Any LAN`` | ``DMZ_IP3``                |                                    |                                                               |
+-------------+----------------------------+------------------------------------+---------------------------------------------------------------+


Inside DMZ
----------

+--------------+-------------+---------------+-------------------+
| From/To      | To/From     | Ports to open | Usage             |
+==============+=============+===============+===================+
| Web Proxy    | SIP Proxy   | - TCP/443     | Websocket for SIP |
|              |             |               |                   |
| ``DMZ_IP1``  | ``DMZ_IP2`` |               |                   |
+--------------+-------------+---------------+-------------------+


This is probably unneeded (traffic between servers inside the DMZ should be in most of the cases
unfiltered), but if it would be the case this flow must be authorized.

