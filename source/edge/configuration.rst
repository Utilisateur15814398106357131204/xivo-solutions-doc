.. _edge_config:

******************
Edge Configuration
******************

.. contents:: :local:

Edge Server configuration
=========================

.. _edge_config_sslcert:

SSL Certificates
----------------

.. note:: This step is to be done on each host

The Edge Solution must be configured with valid certificate for your domain.

You have to put the:

* **fullchain** certificate: here :file:`/etc/docker/ssl/xivo-edge.crt`
  and also here :file:`/etc/docker/ssl/xivo-edge.chain` (this last location is mandatory for the SIP Proxy service)
* certificate key: here :file:`/etc/docker/ssl/xivo-edge.key`


Make sure that the certificate key are given readonly permission to root:

.. code-block:: bash

    chmod 400 /etc/docker/ssl/xivo-edge.key

.. _edge_config_turnsecret:

TURN Server Secret
------------------

You must generate a secret for the TURN server (in the following it will referred to as ``<TURN_SECRET>``).

Generate it with the following command:

.. code:: bash

    openssl rand -hex 16

Keep this ``<TURN_SECRET>``. You will need it to configure

* the TURN Server
* the xucserver on the *XiVO CC/UC*
* and to generate the turn configuration on for asterisk (on XiVO Main and MDS)


3 Servers Configuration
-----------------------

.. note:: Follow this part if you configure the Edge Solution on 3 servers

.. _edge_config_3serversschema:

3 Servers Example Schema
^^^^^^^^^^^^^^^^^^^^^^^^

.. figure:: images/edge_archi_3servers_example.png
   :alt: edge_archi_3servers_example

Web Proxy
^^^^^^^^^

.. note:: This step is to be done on the server which will host the **Web Proxy** (nginx) service

Create the :file:`.env` file with the following variables (replace values accordingly, and see example below):

.. code-block:: ini

    XIVOCC_TAG=2021.07
    XIVOCC_DIST=latest
    XIVOCC_HOST=<IP ADDRESS OF THE XIVO CC/UC (xucserver/xucmgt/nginx server)>
    XIVO_HOST=<IP ADDRESS OF THE XIVO>
    EDGE_FQDN=<XIVO EDGE FQDN>
    EDGE_KAMAILIO_HOST=<IP ADDRESS OF THE KAMAILIO SERVER>

**Example:**

* given you install latest Gaia version
* and given the :ref:`edge_config_3serversschema`

you should come up with:

.. code-block:: ini

    XIVOCC_TAG=2021.07
    XIVOCC_DIST=latest
    XIVOCC_HOST=LAN_IP1
    XIVO_HOST=LAN_IP2
    EDGE_FQDN=edge.mycompany.com
    EDGE_KAMAILIO_HOST=DMZ_IP2


SIP Proxy
^^^^^^^^^

.. note:: This step is to be done on the server which will host the **SIP Proxy** (kamailio) service

Create the :file:`.env` file with the following variables (replace values accordingly, and see example below):

.. code-block:: ini

    XIVOCC_TAG=2021.07
    XIVOCC_DIST=latest
    EDGE_FQDN=<XIVO EDGE FQDN>
    XIVO_HOST=<IP ADDRESS OF THE XIVO>
    XIVO_MDS_HOST_DEFAULT="default: <DATA IP ADDRESS TO MDS DEFAULT>"
    EDGE_HOST_IP=<Edge server IP on which to listen>

.. important:: If you configure the Edge Solution for a XDS installation you must:

     * add to the ``.env`` file a line per MDS with the following format::

         XIVO_MDS_HOST_MDS1="<mds technical name>: <DATA IP ADDRESS TO MDS1>"

       For example: ``XIVO_MDS_HOST_MDS1="mds1: 192.168.240.2"``
     * and add to the ``extra_hosts`` section of the kamailio service the
       variable ``XIVO_MDS_HOST_MDS1``

**Example:**

* given you install latest Gaia version
* and given the :ref:`edge_config_3serversschema`

you should come up with:

.. code-block:: ini

    XIVOCC_TAG=2021.07
    XIVOCC_DIST=latest
    EDGE_FQDN=edge.mycompany.com
    XIVO_HOST=LAN_IP2
    XIVO_MDS_HOST_DEFAULT="default: LAN_IP2"
    XIVO_MDS_HOST_MDS1="mds1: LAN_IP3"
    EDGE_HOST_IP=DMZ_IP2


STUN/TURN server
^^^^^^^^^^^^^^^^

.. note:: This step is to be done on the server which will host the **STUN/TURN Server** (coturn) service

Create the :file:`.env` file with the following variables (replace values accordingly, and see example below):

.. code-block:: ini

    XIVOCC_TAG=2021.07
    XIVOCC_DIST=latest
    EDGE_HOST_IP=<IP OF THE STUN/TURN SERVER>
    TURN_EXTERNAL_IP=<External IP of the edge Server>
    TURN_SERVER_SECRET=<TURN_SECRET>
    TURN_REALM=<domain name of client>

**Example:**

* given you install latest Gaia version
* and given the :ref:`edge_config_3serversschema`

you should come up with:

.. code-block:: ini

    XIVOCC_TAG=2021.07
    XIVOCC_DIST=latest
    EDGE_HOST_IP=DMZ_IP3
    TURN_EXTERNAL_IP=DMZ_IP3
    TURN_SERVER_SECRET=<TURN_SECRET>
    TURN_REALM=mycompany.com


1 Server Configuration
-------------------------

.. note:: Follow this part if you configure the Edge Solution 1 server


.. _edge_config_1serverschema:

1 Server Example Schema
^^^^^^^^^^^^^^^^^^^^^^^

.. figure:: images/edge_archi_1server_example.png
   :alt: edge_archi_1server_example

Web Proxy, SIP Proxy and TURN Server
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Create the :file:`.env` file with the following variables (replace values accordingly, and see example below):

.. code-block:: ini

    XIVOCC_TAG=2021.07
    XIVOCC_DIST=latest
    XIVOCC_HOST=<IP ADDRESS OF THE XIVOCC>
    XIVO_HOST=<IP ADDRESS OF THE XIVO>
    EDGE_FQDN=<EDGE FQDN>
    TURN_SERVER_SECRET=<TURN_SECRET>
    TURN_REALM=<EDGE DOMAIN>
    EDGE_HOST_IP=<EDGE HOST IP>
    TURN_EXTERNAL_IP=<EDGE HOST IP>
    XIVO_MDS_HOST_DEFAULT="default: <DATA IP ADDRESS TO MDS DEFAULT>"

.. important:: If you configure the Edge Solution for a XDS installation you must:

     * add to the ``.env`` file a line per MDS with the following format::

         XIVO_MDS_HOST_MDS1="<mds technical name>: <DATA IP ADDRESS TO MDS1>"

       For example: ``XIVO_MDS_HOST_MDS1="mds1: 192.168.240.2"``
     * and add to the ``extra_hosts`` section of the kamailio service the
       variable ``XIVO_MDS_HOST_MDS1``


**Example:**

* given you install latest Gaia version
* and given the :ref:`edge_config_1serverschema`

you should come up with:

.. code-block:: ini

    XIVOCC_TAG=2021.07
    XIVOCC_DIST=latest
    XIVOCC_HOST=LAN_IP1
    XIVO_HOST=LAN_IP2
    EDGE_FQDN=edge.mycompany.com
    TURN_SERVER_SECRET=<TURN_SECRET>
    TURN_REALM=mycompany.com
    EDGE_HOST_IP=DMZ_IP1
    TURN_EXTERNAL_IP=DMZ_IP1
    XIVO_MDS_HOST_DEFAULT="default: LAN_IP2"
    XIVO_MDS_HOST_DEFAULT="mds1: LAN_IP3"


Start the services
------------------

To start the services run the following command (you have to run it on all servers if you are in a 3 servers installation):

.. code:: bash

    edge-dcomp up -d

XiVO CC/UC configuration
========================

.. note:: These steps are to be done on the XiVO CC/UC

On XiVO CC/UC server (the server which hosts the xucmgt/xucserver and nginx) add the following variables in the ``custom.env`` file:

* change XUC_HOST to the EDGE FQDN
* and add the TURN_SERVER_SECRET with the ``<TURN_SECRET>`` value
  (the ``<TURN_SECRET>`` is the secret generated for during :ref:`edge_config_turnsecret`)

.. code-block:: ini

    XUC_HOST=<XIVO EDGE FQDN>
    TURN_SERVER_SECRET=<TURN_SECRET>

* then you need to restart the services

  .. code-block:: bash

    xivocc-dcomp restart


XiVO Configuration
==================

.. note:: These steps are to be done on the XiVO

STUN/TURN server configuration
------------------------------

On the XiVO you need to configure the STUN/TURN server address.

.. warning:: Your Edge installation must be up and running when you do this step.
    As soon as you change this configuration all WebRTC users **won't work** without the Edge Solution.

* In the XiVO Admin webi :menuselection:`Services -> IPBX -> General Settings -> SIP Protocol`
  tab **Network**
* In parameter :guilabel:`XiVO Edge FQDN` enter the **TURN server**
  address with format ``FQDN:3478`` or ``IP:3478`` (you must put port **3478**).
  **Warning:** this FQDN must be resolvable and reachable from the XiVO & Public Internet.

  * 1 Server deployment: use the server public IP address or FQDN (i.e. ``edge.mycompany.com`` or ``DMZ_IP1`` in 1 Server Schema)
  * 3 Servers deployment: use the TURN server public IP address or FQDN (i.e. ``DMZ_IP3`` in 3 Servers Schema)
* Click save: it will reload the SIP and the RTP configuration of the XiVO (Main and MDS if any)
* Then **on each XiVO/MDS** run the following script to generate the credentials for the TURN server
  (the ``<TURN_SECRET>`` is the secret generated during :ref:`edge_config_turnsecret`):

  .. code-block:: bash

      xivo-edge-gen-turn-cred <TURN_SECRET>

* This script will generate new turn username and password in :file:`/etc/asterisk/rtp.d/01-xivo-edge-turn-cred.conf`
* You then need to reload the rtp configuration

  .. code-block:: bash

    asterisk -rx 'module reload res_rtp_asterisk.so'


.. note:: The turn address ``turnaddr`` is generated by confgend (see conf in file :file:`/etc/asterisk/rtp.conf`).

Fail2ban
--------

.. important:: If you configure the Edge Solution for a XDS installation you must do it on each MDS.

You need to add Kamailio's ip address to protect it from
being banned and resulting in complete block of webrtc users.

* Edit file :file:`/etc/fail2ban/filter.d/asterisk-xivo.conf` and add to the ``ignoreregex`` section:

  * In *3 Servers Schema* replace ``<SIP_PROXY_HOST_IP>`` by ``DMZ_IP2``
  * In *1 Server Schema* replace ``<SIP_PROXY_HOST_IP>`` by ``DMZ_IP1``

  .. code-block:: ini

      ignoreregex = <SIP_PROXY_HOST_IP>


* Finally restart fail2ban service:

  .. code-block:: bash

    systemctl restart fail2ban.service



