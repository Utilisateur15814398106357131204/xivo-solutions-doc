**********
PostgreSQL
**********

Container keeps on restarting after upgrade
===========================================

After upgrading Docker it can sometimes happen that the container *xivocc_pgxivocc_1* gets stuck in restarting mode. Logs show the following error

::

   FATAL:  data directory "/var/lib/postgresql/data" has group or world access
   DETAIL:  Permissions should be u=rwx (0700).

Error is caused by wrong permissions on the PostgreSQL data directory. Follow the below steps in order to fix the issue:

#. Find the location of the directory inside the container information::

    docker inspect xivocc_pgxivocc_1 | grep volumes

#. Command output should give something like this::

    "Source": "/var/lib/docker/volumes/82898d65ae41174865211ff709c12b1f5e7b3c1b7d26e73500b6e6c7cffb3f10/_data",

#. Use the result above to change the permissions::

    chmod -R 700 /var/lib/docker/volumes/82898d65ae41174865211ff709c12b1f5e7b3c1b7d26e73500b6e6c7cffb3f10/_data

#. Restart the container::

    xivocc-dcomp restart pgxivocc
